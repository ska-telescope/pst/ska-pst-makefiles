# SKA PST MAKEFILES
Team PST common makefile repository

## Motivation
To centralise team PST specific common makefile targets.

## Usage
1. Add the repository as a submodule.
        
        git submodule add https://gitlab.com/ska-telescope/pst/ska-pst-makefiles.git .pst

1. Sync the submodule 

        git submodule sync .pst

## Make targets
 - local-dev-env
 - local-cpp-build-debug
 - local-cpp-build-export-compile-commands
 - local-cpp-build-release
 - local-cpp-ci-simulation
 - local-cpp-ci-simulation-coverage
 - local-cpp-ci-simulation-installation   
 - local-cpp-ci-simulation-lint
 - local-cpp-ci-simulation-test
 - local-cpp-clean-buildpath
 - local-cpp-gcovr
 - local-cpp-gcovr-html
 - local-cpp-gcovr-xml
 - local-cpp-lint
 - local-cpp-lint-clang
 - local-cpp-lint-cppcheck
 - local-cpp-lint-iwyu
 - local-cpp-test-ctest
 - local-cpp-test-ctest-memcheck
 - local-cpp-test-installation
