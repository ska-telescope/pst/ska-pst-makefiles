.PHONY: local-docker-build local-docker-run local-notebook-test-env

# DEV_IMAGE=? # Declare in PrivateRules.mak for invoking local development environment local-dev-env
# DEV_TAG=`grep -m 1 -o "[0-9].*" .release` # Declare in PrivateRules.mak for invoking local development environment local-dev-env

DSPSR_TEST_DATA ?= $(shell test -d /data/dspsr-functional-pipeline-tests && echo "-v /data:/data")

local-dev-env:
	@echo 'PROJECT: $(PROJECT)'
	@echo 'DEV_IMAGE: $(DEV_IMAGE)'
	@echo 'DEV_TAG: $(DEV_TAG)'
	@echo 'DSPSR_TEST_DATA: $(DSPSR_TEST_DATA)'
	@$(OCI_BUILDER) run --rm -ti --ulimit memlock=268435456:-1 -v $(PWD):/mnt/$(PROJECT) -w /mnt/$(PROJECT) $(DSPSR_TEST_DATA) $(DEV_IMAGE):$(DEV_TAG) bash

local-docker-run:
	docker run -it --rm -v $(PWD):/mnt/$(PROJECT) -t $(strip $(OCI_IMAGE)):$(VERSION) $(RUN_ARGS)

ifeq (local-docker-run,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif

local-notebook-test-env:
	@echo 'SKA_PST_PYTHON_BUILDER_IMAGE: $(SKA_PST_PYTHON_BUILDER_IMAGE)'
	@$(OCI_BUILDER) run --rm -ti -v $(PWD):/mnt/$(PROJECT) -w /mnt/$(PROJECT) $(SKA_PST_PYTHON_BUILDER_IMAGE) bash

